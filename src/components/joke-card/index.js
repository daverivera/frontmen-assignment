import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
// import { Card } from 'react-md';

import FavoriteStar from '../favorite-star';

import './style.scss';

class JokeCard extends PureComponent {
    static propTypes = {
        id: PropTypes.number.isRequired,
        isFavorite: PropTypes.bool.isRequired,
        joke: PropTypes.string.isRequired,
        onClick: PropTypes.func.isRequired
    }

    handleFavoriteSelection = () => {
        const {
            id,
            isFavorite,
            joke,
            onClick
        } = this.props;

        onClick({ id, joke, isFavorite });
    }

    render() {
        const { isFavorite, joke } = this.props;

        return (
            <div className="joke-card">
                <div className="joke-card__favorite" onClick={this.handleFavoriteSelection}>
                    <FavoriteStar isFavorite={isFavorite} />
                </div>
                <div className="joke-card__joke">
                    {joke}
                </div>
            </div>
        );
    }
}

export default JokeCard;
