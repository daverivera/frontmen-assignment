import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import './style.scss';

class TextField extends PureComponent {
    static propTypes = {
        className: PropTypes.string,
        error: PropTypes.oneOfType([
            PropTypes.bool,
            PropTypes.number
        ]),
        onChange: PropTypes.func,
        placeholder: PropTypes.string,
        type: PropTypes.oneOf(['text', 'password']),
        value: PropTypes.string
    }

    static defaultProps = {
        error: false,
        className: '',
        onChange: () => {},
        placeholder: undefined,
        type: 'text',
        value: ''
    }

    changeFilterValue = (e) => {
        const { onChange } = this.props;
        if (onChange) {
            onChange(e.target.value, e);
        }
    }

    render() {
        const {
            className,
            error,
            placeholder,
            type,
            value
        } = this.props;

        return (
            <input
                className={classNames(
                    'text-field',
                    {
                        'text-field--error': error
                    },
                    className
                )}
                onChange={this.changeFilterValue}
                placeholder={placeholder}
                type={type}
                value={value}
            />
        );
    }
}

export default TextField;
