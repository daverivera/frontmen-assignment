import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import JokeCard from '../joke-card';

import './style.scss';

class JokesList extends PureComponent {
    static propTypes = {
        jokes: PropTypes.arrayOf(PropTypes.shape({
            id: PropTypes.number.isRequired,
            isFavorite: PropTypes.bool.isRequired,
            joke: PropTypes.string.isRequired
        })).isRequired,
        selectFavorite: PropTypes.func.isRequired
    }

    renderJokes() {
        const { jokes, selectFavorite } = this.props;

        return jokes.map(({
            categories,
            id,
            isFavorite,
            joke
        }) => (
            <JokeCard
                categories={categories}
                id={id}
                isFavorite={isFavorite}
                joke={joke}
                key={id}
                onClick={selectFavorite}
            />
        ));
    }

    render() {
        return (
            <div className="jokes-list">
                {this.renderJokes()}
            </div>
        );
    }
}

export default JokesList;
