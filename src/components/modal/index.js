import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import './style.scss';

class Modal extends PureComponent {
    static propTypes = {
        children: PropTypes.node.isRequired,
        visible: PropTypes.bool.isRequired
    }

    render() {
        const {
            children,
            visible
        } = this.props;

        return (
            <div className={classNames('modal', { 'modal--visible': visible })}>
                <div className="modal__background" />
                <div className="modal__content">
                    <div className="modal__box">
                        {children}
                    </div>
                </div>
            </div>
        );
    }
}

export default Modal;
