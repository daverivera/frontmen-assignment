import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';

import './style.scss';

const FavoriteStar = ({ isFavorite }) => {
    const starName = isFavorite ? 'star' : 'star_border';

    return (
        <i className={classNames(
            'material-icons',
            {
                'favorite-star--active': isFavorite
            }
        )}
        >
            {starName}
        </i>
    );
};

FavoriteStar.propTypes = {
    isFavorite: PropTypes.bool.isRequired
};

export default FavoriteStar;
