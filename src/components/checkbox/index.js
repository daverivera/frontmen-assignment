import React from 'react';
import PropTypes from 'prop-types';

import './style.scss';

const Checkbox = ({ disabled, onChange, value }) => (
    <input
        disabled={disabled}
        className="checkbox"
        onChange={onChange}
        type="checkbox"
        checked={value}
    />
);

Checkbox.propTypes = {
    disable: PropTypes.bool,
    onChange: PropTypes.func,
    value: PropTypes.bool
};

Checkbox.defaultProps = {
    disabled: false,
    onChange: () => {},
    value: ''
};

export default Checkbox;
