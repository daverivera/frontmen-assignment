import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import './style.scss';

const Button = ({
    children,
    className,
    disabled,
    onClick,
    primary
}) => (
    <button
        className={classNames(
            'button',
            {
                'button--disabled': disabled,
                'button--primary': primary && !disabled
            },
            className
        )}
        disabled={disabled}
        onClick={onClick}
        type="button"
    >
        {children}
    </button>
);

Button.propTypes = {
    children: PropTypes.node,
    className: PropTypes.string,
    disabled: PropTypes.bool,
    onClick: PropTypes.func,
    primary: PropTypes.bool
};

Button.defaultProps = {
    children: null,
    className: '',
    disabled: false,
    onClick: () => {},
    primary: false
};

export default Button;
