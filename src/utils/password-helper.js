import { PASSWORD_BUSINESS_RULES } from './constants';

const validatePassword = (password) => {
    const errors = [];

    if (password.length > PASSWORD_BUSINESS_RULES.maxLength.value) {
        errors.push(PASSWORD_BUSINESS_RULES.maxLength.message);
    }

    if (PASSWORD_BUSINESS_RULES.onlyLowerCase.value.test(password)) {
        errors.push(PASSWORD_BUSINESS_RULES.onlyLowerCase.message);
    }

    if (!PASSWORD_BUSINESS_RULES.overlappingPairs.value.test(password)) {
        errors.push(PASSWORD_BUSINESS_RULES.overlappingPairs.message);
    }

    if (PASSWORD_BUSINESS_RULES.unacceptedLetters.value.test(password)) {
        errors.push(PASSWORD_BUSINESS_RULES.unacceptedLetters.message);
    }

    if (!PASSWORD_BUSINESS_RULES.increasingStraightSeries.value.test(password)) {
        errors.push(PASSWORD_BUSINESS_RULES.increasingStraightSeries.message);
    }

    return errors;
};

export default {
    validatePassword
};
