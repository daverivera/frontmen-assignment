import unescape from 'unescape';

import { MAXIMUM_NUMBER_FAVORITE_JOKES } from './constants';

const isJokeAlreadyAdded = jokeId => ({ id: favoriteId }) => favoriteId === jokeId;

const addFavorite = (favorites, { id, joke }) => {
    const isJokeAlreadyFavorite = !!favorites.find(isJokeAlreadyAdded(id));
    const isListInMaximumCapacity = favorites.length === MAXIMUM_NUMBER_FAVORITE_JOKES;

    if (isListInMaximumCapacity && !isJokeAlreadyFavorite) {
        return favorites;
    }

    return isJokeAlreadyFavorite // Then filter out the favorite jokes
        ? favorites.filter(favoriteJoke => !isJokeAlreadyAdded(id)(favoriteJoke))
        : favorites.concat({
            id,
            isFavorite: true,
            joke
        });
};

const processJokes = (jokes, favorites) =>
    jokes
        .map(({ id, joke }) => ({
            id,
            isFavorite: !!favorites.find(isJokeAlreadyAdded(id)),
            joke: unescape(joke)
        }));

const appendSingleJoke = (jokesList, [{ id, joke }]) => {
    const isExistentJoke = !!jokesList.find(isJokeAlreadyAdded(id));
    const isListInMaximumCapacity = jokesList.length === MAXIMUM_NUMBER_FAVORITE_JOKES;

    if (isExistentJoke || isListInMaximumCapacity) {
        return jokesList;
    }

    return jokesList.concat({
        id,
        isFavorite: false,
        joke: unescape(joke)
    });
};

export default {
    addFavorite,
    appendSingleJoke,
    processJokes
};
