import { SESSION_STORAGE_KEYS } from './constants';

const storeSession = user => sessionStorage.setItem(SESSION_STORAGE_KEYS.loginSession, btoa(user));

const isUserLoggedIn = () => !!sessionStorage.getItem(SESSION_STORAGE_KEYS.loginSession);

export default {
    isUserLoggedIn,
    storeSession
};
