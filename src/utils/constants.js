export const API_URLS = {
    randomJokes: 'http://api.icndb.com/jokes/random'
};

export const CATEGORIES = {
    favorites: 1,
    randomJokes: 2
};

export const DEFAULT_SECONDS_TO_REFRESH = 5;

export const MAXIMUM_NUMBER_FAVORITE_JOKES = 10;

export const MAX_FETCHED_JOKES_NUMBER = 10;

export const MIN_FETCHED_JOKES_NUMBER = 1;

export const PASSWORD_BUSINESS_RULES = {
    maxLength: {
        message: 'Passwords cannot be longer than 32 characters.',
        value: 32
    },

    onlyLowerCase: {
        message: 'Passwords can only contain lower case alphabetic characters.',
        value: /(?=.*[A-Z0-9])/
    },

    overlappingPairs: {
        message: 'Passwords must contain at least two non-overlapping pairs of letters, like aa, bb, or cc.',
        value: /(\w)\1/
    },

    unacceptedLetters: {
        message: 'Passwords may not contain the letters i, O, or l, as these letters can be mistaken for other characters and are therefore confusing.',
        value: /(O|i|l)+/
    },

    increasingStraightSeries: {
        message: 'Passwords must include one increasing straight of at least three letters, like abc, cde, fgh, and so on, up to xyz. They cannot skip letters; acd doesn\'t count.',
        value: /(abc|bcd|cde|def|efg|fgh|ghi|hij|ijk|jkl|klm|lmn|mno|nop|opq|pqr|qrs|rst|stu|tuv|uvw|vwx|wxy|xyz)+/
    }
};

export const SESSION_STORAGE_KEYS = {
    loginSession: 'loginSession'
};
