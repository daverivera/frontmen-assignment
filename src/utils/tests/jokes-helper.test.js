import JokesHelper from '../jokes-helper';

describe('Jokes Helper', () => {
    describe('processJokes', () => {
        describe('when a list of jokes is passed', () => {
            describe('and the list of favorites is empty', () => {
                test('should return a new list of jokes with isFavorite as `false` and without categories per joke', () => {
                    const jokesList = [{
                        id: 51,
                        joke: 'Coroners refer to dead people as &quot;ABC\'s&quot;. Already Been Chucked.',
                        categories: []
                    }, {
                        id: 533,
                        joke: 'The Chuck Norris Eclipse plugin made alien contact.',
                        categories: ['nerdy']
                    }];

                    const emptyFavoriteList = [];

                    expect(JokesHelper.processJokes(jokesList, emptyFavoriteList))
                        .toEqual([{
                            id: 51,
                            isFavorite: false,
                            joke: 'Coroners refer to dead people as "ABC\'s". Already Been Chucked.'
                        }, {
                            id: 533,
                            isFavorite: false,
                            joke: 'The Chuck Norris Eclipse plugin made alien contact.'
                        }]);
                });
            });

            describe('and the list of favorites is not empty ', () => {
                test('should return a new list of jokes with isFavorite as `true` for the selected favorites, false for the non selected favorites and without categories per joke', () => {
                    const jokesList = [{
                        id: 51,
                        joke: 'The quickest way to a man\'s heart is with Chuck Norris\' fist.',
                        categories: []
                    }, {
                        id: 533,
                        joke: 'The Chuck Norris Eclipse plugin made alien contact.',
                        categories: ['nerdy']
                    }];

                    const selectedFavoriteList = [{
                        id: 533,
                        isFavorite: true,
                        joke: 'The Chuck Norris Eclipse plugin made alien contact.'
                    }];

                    expect(JokesHelper.processJokes(jokesList, selectedFavoriteList))
                        .toEqual([{
                            id: 51,
                            isFavorite: false,
                            joke: 'The quickest way to a man\'s heart is with Chuck Norris\' fist.'
                        }, {
                            id: 533,
                            isFavorite: true,
                            joke: 'The Chuck Norris Eclipse plugin made alien contact.'
                        }]);
                });
            });
        });
    });

    describe('addFavorite', () => {
        describe('when adding a favorite Joke to the list', () => {
            describe('and the favorites list is empty', () => {
                test('should append the object', () => {
                    const emptyFavoriteList = [];

                    const newFavoriteJoke = {
                        id: 533,
                        isFavorite: false,
                        joke: 'The Chuck Norris Eclipse plugin made alien contact.'
                    };

                    expect(JokesHelper.addFavorite(emptyFavoriteList, newFavoriteJoke))
                        .toEqual([{
                            id: 533,
                            isFavorite: true,
                            joke: 'The Chuck Norris Eclipse plugin made alien contact.'
                        }]);
                });
            });

            describe('and the favorites list is not empty', () => {
                describe('and the new favorite joke is not in the list', () => {
                    test('should append the joke at the end', () => {
                        const nonRepeatedFavoriteJokesList = [{
                            id: 51,
                            isFavorite: false,
                            joke: 'The quickest way to a man\'s heart is with Chuck Norris\' fist.'
                        }];

                        const newFavoriteJoke = {
                            id: 533,
                            isFavorite: true,
                            joke: 'The Chuck Norris Eclipse plugin made alien contact.'
                        };

                        expect(JokesHelper.addFavorite(nonRepeatedFavoriteJokesList, newFavoriteJoke))
                            .toEqual([{
                                id: 51,
                                isFavorite: false,
                                joke: 'The quickest way to a man\'s heart is with Chuck Norris\' fist.'
                            }, {
                                id: 533,
                                isFavorite: true,
                                joke: 'The Chuck Norris Eclipse plugin made alien contact.'
                            }]);
                    });
                });

                describe('and the new favorite joke already exists in the list', () => {
                    test('should remove the joke from the favorites list', () => {
                        const nonRepeatedFavoriteJokesList = [{
                            id: 51,
                            isFavorite: false,
                            joke: 'The quickest way to a man\'s heart is with Chuck Norris\' fist.'
                        }, {

                            id: 533,
                            isFavorite: true,
                            joke: 'The Chuck Norris Eclipse plugin made alien contact.'
                        }];

                        const newFavoriteJoke = {
                            id: 533,
                            isFavorite: true,
                            joke: 'The Chuck Norris Eclipse plugin made alien contact.'
                        };

                        expect(JokesHelper.addFavorite(nonRepeatedFavoriteJokesList, newFavoriteJoke))
                            .toEqual([{
                                id: 51,
                                isFavorite: false,
                                joke: 'The quickest way to a man\'s heart is with Chuck Norris\' fist.'
                            }]);
                    });
                });

                describe('and the list hast already reached the maximum number of favorites, which is 10', () => {
                    describe('and the joke is in the not on the favorites list', () => {
                        test('should maintain the list without alterations and return it', () => {
                            const fullFavoritesList = [
                                { id: 0 },
                                { id: 1 },
                                { id: 2 },
                                { id: 3 },
                                { id: 4 },
                                { id: 5 },
                                { id: 6 },
                                { id: 7 },
                                { id: 8 },
                                { id: 9 }
                            ];

                            const newFavoriteJoke = {
                                id: 533,
                                isFavorite: true,
                                joke: 'The Chuck Norris Eclipse plugin made alien contact.'
                            };

                            expect(JokesHelper.addFavorite(fullFavoritesList, newFavoriteJoke))
                                .toEqual([
                                    { id: 0 },
                                    { id: 1 },
                                    { id: 2 },
                                    { id: 3 },
                                    { id: 4 },
                                    { id: 5 },
                                    { id: 6 },
                                    { id: 7 },
                                    { id: 8 },
                                    { id: 9 }
                                ]);
                        });
                    });

                    describe('and the joke is in the favorites list', () => {
                        test('should remove the joke from the list', () => {
                            const fullFavoritesList = [
                                { id: 0 },
                                { id: 1 },
                                { id: 2 },
                                { id: 3 },
                                { id: 4 },
                                { id: 5 },
                                { id: 6 },
                                { id: 7 },
                                { id: 8 },
                                { id: 9 }
                            ];

                            const newFavoriteJoke = { id: 5 };

                            expect(JokesHelper.addFavorite(fullFavoritesList, newFavoriteJoke))
                                .toEqual([
                                    { id: 0 },
                                    { id: 1 },
                                    { id: 2 },
                                    { id: 3 },
                                    { id: 4 },
                                    { id: 6 },
                                    { id: 7 },
                                    { id: 8 },
                                    { id: 9 }
                                ]);
                        });
                    });
                });
            });
        });
    });

    describe('appendSingleJoke', () => {
        describe('when fetching one single joke', () => {
            describe('and the joke is not repeated', () => {
                describe('and list is not full', () => {
                    test('should append joke to the end of the list', () => {
                        const jokesList = [{
                            id: 51,
                            isFavorite: false,
                            joke: 'The quickest way to a man\'s heart is with Chuck Norris\' fist.'
                        }];

                        const nonRepeatedJoke = [{
                            id: 533,
                            joke: 'The Chuck Norris Eclipse plugin made alien contact.',
                            categories: ['nerdy']
                        }];

                        expect(JokesHelper.appendSingleJoke(jokesList, nonRepeatedJoke))
                            .toEqual([{
                                id: 51,
                                isFavorite: false,
                                joke: 'The quickest way to a man\'s heart is with Chuck Norris\' fist.'
                            }, {
                                id: 533,
                                isFavorite: false,
                                joke: 'The Chuck Norris Eclipse plugin made alien contact.'
                            }]);
                    });
                });

                describe('and list is full', () => {
                    test('should return the same list', () => {
                        const fullList = [
                            { id: 0 },
                            { id: 1 },
                            { id: 2 },
                            { id: 3 },
                            { id: 4 },
                            { id: 5 },
                            { id: 6 },
                            { id: 7 },
                            { id: 8 },
                            { id: 9 }
                        ];

                        const nonRepeatedJoke = [{ id: 533 }];

                        expect(JokesHelper.appendSingleJoke(fullList, nonRepeatedJoke))
                            .toEqual([
                                { id: 0 },
                                { id: 1 },
                                { id: 2 },
                                { id: 3 },
                                { id: 4 },
                                { id: 5 },
                                { id: 6 },
                                { id: 7 },
                                { id: 8 },
                                { id: 9 }
                            ]);
                    });
                });
            });

            describe('and the joke is already in the list', () => {
                test('should return the list as is', () => {
                    const jokesList = [{
                        id: 51,
                        isFavorite: false,
                        joke: 'The quickest way to a man\'s heart is with Chuck Norris\' fist.'
                    }, {
                        id: 533,
                        isFavorite: false,
                        joke: 'The Chuck Norris Eclipse plugin made alien contact.'
                    }];

                    const repeatedJoke = [{
                        id: 533,
                        joke: 'The Chuck Norris Eclipse plugin made alien contact.',
                        categories: ['nerdy']
                    }];

                    expect(JokesHelper.appendSingleJoke(jokesList, repeatedJoke))
                        .toEqual([{
                            id: 51,
                            isFavorite: false,
                            joke: 'The quickest way to a man\'s heart is with Chuck Norris\' fist.'
                        }, {
                            id: 533,
                            isFavorite: false,
                            joke: 'The Chuck Norris Eclipse plugin made alien contact.'
                        }]);
                });
            });
        });
    });
});
