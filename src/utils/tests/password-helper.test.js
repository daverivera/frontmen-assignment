import PasswordHelper from '../password-helper';
import { PASSWORD_BUSINESS_RULES } from '../constants';

describe('Password Helper', () => {
    describe('validatePassword', () => {
        describe('when the password is valid', () => {
            test('should return an empty array of errors', () => {
                const validPassword = 'aabcdd';

                expect(PasswordHelper.validatePassword(validPassword))
                    .toEqual([]);
            });
        });

        describe('when verifying "maxLength" rule', () => {
            describe('and password is longer than 32 characters', () => {
                test('should return "maxLength" message in the errors list array', () => {
                    const thirtyThreeCharacterPassword = 'Lorem ipsum dolor sit amet, conse';

                    expect(PasswordHelper.validatePassword(thirtyThreeCharacterPassword))
                        .toContain(PASSWORD_BUSINESS_RULES.maxLength.message);
                });
            });

            describe('and password is less than 32 characters', () => {
                test('should not return "maxLength" message in the errors list array', () => {
                    const thirtyTwoCharacterPassword = 'Lorem ipsum dolor sit amet, cons';

                    expect(PasswordHelper.validatePassword(thirtyTwoCharacterPassword))
                        .not.toContain(PASSWORD_BUSINESS_RULES.maxLength.message);
                });
            });
        });

        describe('when verifying "onlyLowerCase" rule', () => {
            describe('and password has at least one upper case', () => {
                test('should return "onlyLowerCase" message in the errors list array', () => {
                    const uppercasePassword = 'Lorem Ipsum';

                    expect(PasswordHelper.validatePassword(uppercasePassword))
                        .toContain(PASSWORD_BUSINESS_RULES.onlyLowerCase.message);
                });
            });

            describe('and password has non-alphabetic characters', () => {
                test('should return "onlyLowerCase" message in the errors list array', () => {
                    const uppercasePassword = 'l0rem ipsum';

                    expect(PasswordHelper.validatePassword(uppercasePassword))
                        .toContain(PASSWORD_BUSINESS_RULES.onlyLowerCase.message);
                });
            });

            describe('and password has no upper case characters', () => {
                test('should not return "onlyLowerCase" message in the errors list array', () => {
                    const allLowercasePassword = 'lorem ipsum';

                    expect(PasswordHelper.validatePassword(allLowercasePassword))
                        .not.toContain(PASSWORD_BUSINESS_RULES.onlyLowerCase.message);
                });
            });
        });

        describe('when verifying "overlappingPairs" rule', () => {
            describe('and password does not have at least two non-overlapping pairs of letters', () => {
                test('should return "overlappingPairs" message in the errors list array', () => {
                    const notNonOverlappingPassword = 'lorem ipsum';

                    expect(PasswordHelper.validatePassword(notNonOverlappingPassword))
                        .toContain(PASSWORD_BUSINESS_RULES.overlappingPairs.message);
                });
            });

            describe('and password has at least two non-overlapping pairs of letters', () => {
                test('should not return "overlappingPairs" message in the errors list array', () => {
                    const nonOverlappingPassword = 'lloremm';

                    expect(PasswordHelper.validatePassword(nonOverlappingPassword))
                        .not.toContain(PASSWORD_BUSINESS_RULES.overlappingPairs.message);
                });
            });
        });

        describe('when verifying "unacceptedLetters" rule', () => {
            describe('and password has an "i" letter', () => {
                test('should return "unacceptedLetters" message in the errors list array', () => {
                    const unacceptedLettersPassword = 'ipsum';

                    expect(PasswordHelper.validatePassword(unacceptedLettersPassword))
                        .toContain(PASSWORD_BUSINESS_RULES.unacceptedLetters.message);
                });
            });

            describe('and password has an "O" letter', () => {
                test('should return "unacceptedLetters" message in the errors list array', () => {
                    const unacceptedLettersPassword = 'Orem';

                    expect(PasswordHelper.validatePassword(unacceptedLettersPassword))
                        .toContain(PASSWORD_BUSINESS_RULES.unacceptedLetters.message);
                });
            });

            describe('and password has an "l" letter', () => {
                test('should return "unacceptedLetters" message in the errors list array', () => {
                    const unacceptedLettersPassword = 'lrem';

                    expect(PasswordHelper.validatePassword(unacceptedLettersPassword))
                        .toContain(PASSWORD_BUSINESS_RULES.unacceptedLetters.message);
                });
            });

            describe('and password has no "i", "O" or "l"', () => {
                test('should not return "unacceptedLetters" message in the errors list array', () => {
                    const allAcceptedLettersPassword = 'rem psum';

                    expect(PasswordHelper.validatePassword(allAcceptedLettersPassword))
                        .not.toContain(PASSWORD_BUSINESS_RULES.unacceptedLetters.message);
                });
            });
        });

        describe('when verifying "increasingStraightSeries" rule', () => {
            describe('and password has 3 non consecutive sequential letters', () => {
                test('should return "increasingStraightSeries" message in the errors list array', () => {
                    const nonSequentialPassword = 'acd';

                    expect(PasswordHelper.validatePassword(nonSequentialPassword))
                        .toContain(PASSWORD_BUSINESS_RULES.increasingStraightSeries.message);
                });
            });

            describe('and password has 3 consecutive sequential letters', () => {
                test('should return "increasingStraightSeries" message in the errors list array', () => {
                    const sequentialPassword = 'xyz';

                    expect(PasswordHelper.validatePassword(sequentialPassword))
                        .not.toContain(PASSWORD_BUSINESS_RULES.increasingStraightSeries.message);
                });
            });
        });
    });
});
