import axios from 'axios';

import JokesHelper from './jokes-helper';
import SessionHelper from './session-helper';
import {
    API_URLS,
    MAX_FETCHED_JOKES_NUMBER,
    MIN_FETCHED_JOKES_NUMBER
} from './constants';

axios.interceptors.request.use((config) => {
    // If user is not logged in, show the dialog and prevent the call from being made
    if (!SessionHelper.isUserLoggedIn()) {
        window.location.reload();
        throw new axios.Cancel('Operation canceled by the user.');
    }

    return config;
});

const fetchJokesList = favorites =>
    axios(`${API_URLS.randomJokes}/${MAX_FETCHED_JOKES_NUMBER}`)
        .then(({ data }) => JokesHelper.processJokes(data.value, favorites));

const fetchSingleJoke = jokes =>
    axios(`${API_URLS.randomJokes}/${MIN_FETCHED_JOKES_NUMBER}`)
        .then(({ data }) => JokesHelper.appendSingleJoke(jokes, data.value));

export default { fetchJokesList, fetchSingleJoke };
