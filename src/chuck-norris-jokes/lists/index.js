import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import Button from '../../components/button';
import Checkbox from '../../components/checkbox';
import JokesList from '../../components/jokes-list';
import { CATEGORIES } from '../../utils/constants';

import './style.scss';

class Lists extends Component {
    static propTypes = {
        activeCategory: PropTypes.number.isRequired,
        disableAutoRefresh: PropTypes.bool.isRequired,
        favorites: PropTypes.arrayOf(PropTypes.shape({
            id: PropTypes.number.isRequired,
            isFavorite: PropTypes.bool.isRequired,
            joke: PropTypes.string.isRequired
        })).isRequired,
        hasAutoRefresh: PropTypes.bool.isRequired,
        jokes: PropTypes.arrayOf(PropTypes.shape({
            id: PropTypes.number.isRequired,
            isFavorite: PropTypes.bool.isRequired,
            joke: PropTypes.string.isRequired
        })).isRequired,
        refreshJokesList: PropTypes.func.isRequired,
        selectFavorite: PropTypes.func.isRequired,
        setAutoRefresh: PropTypes.func.isRequired,
        showFavorites: PropTypes.func.isRequired,
        showRandomJokes: PropTypes.func.isRequired,
        timeToRefresh: PropTypes.number.isRequired
    }


    renderFavorites() {
        const { activeCategory, favorites, selectFavorite } = this.props;

        return (
            <div className={classNames(
                'lists__favorites',
                { 'lists__favorites--visible': activeCategory === CATEGORIES.favorites }
            )}
            >
                <JokesList
                    jokes={favorites}
                    selectFavorite={selectFavorite}
                />
            </div>
        );
    }

    renderRandomJokes() {
        const { activeCategory, jokes, selectFavorite } = this.props;

        return (
            <div className={classNames(
                'lists__random-jokes',
                { 'lists__random-jokes--visible': activeCategory === CATEGORIES.randomJokes }
            )}
            >
                <JokesList
                    jokes={jokes}
                    selectFavorite={selectFavorite}
                />
            </div>
        );
    }

    renderRemainingSecondsToRefresh() {
        const { hasAutoRefresh, timeToRefresh } = this.props;

        if (!hasAutoRefresh) {
            return null;
        }

        return `(${timeToRefresh})`;
    }

    render() {
        const {
            activeCategory,
            disableAutoRefresh,
            favorites,
            hasAutoRefresh,
            refreshJokesList,
            setAutoRefresh,
            showFavorites,
            showRandomJokes
        } = this.props;

        return (
            <div className="lists">
                <div className="lists__controls">
                    <div>
                        <Button
                            className="lists__controls-button"
                            onClick={showRandomJokes}
                            primary={activeCategory === CATEGORIES.randomJokes}
                        >
                            Random Jokes
                        </Button>

                        <Button
                            className="lists__controls-button"
                            onClick={showFavorites}
                            primary={activeCategory === CATEGORIES.favorites}
                        >
                            {`Favorites (${favorites.length})`}
                        </Button>
                    </div>
                    <div>

                        [ {this.renderRemainingSecondsToRefresh()} <Checkbox value={hasAutoRefresh} onChange={setAutoRefresh} disabled={disableAutoRefresh}/> auto ]
                        <Button
                            className="lists__controls-button"
                            onClick={refreshJokesList}
                        >
                            Refresh
                        </Button>
                    </div>
                </div>
                <div className="lists__content">
                    {this.renderRandomJokes()}
                    {this.renderFavorites()}
                </div>
            </div>
        );
    }
}

export default Lists;
