import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Button from '../../components/button';
import Modal from '../../components/modal';
import PasswordHelper from '../../utils/password-helper';
import TextField from '../../components/text-field';

import './style.scss';

class Login extends Component {
    static propTypes = {
        onSuccesfulLogin: PropTypes.func.isRequired,
        isVisible: PropTypes.bool.isRequired
    }

    state = {
        errors: [],
        password: '',
        username: ''
    }

    login = () => {
        const { password, username } = this.state;
        const { onSuccesfulLogin } = this.props;

        const errors = PasswordHelper.validatePassword(password);

        this.setState(
            {
                errors,
                password: '',
                username: errors ? username : ''
            },
            () => {
                if (!errors.length) {
                    onSuccesfulLogin(username);
                }
            }
        );
    }

    onPasswordChange = (password) => {
        this.setState({ password });
    }

    onUsernameChange = (username) => {
        this.setState({ username });
    }

    renderErrors() {
        const { errors } = this.state;

        if (!errors.length) {
            return null;
        }

        const errorsList = errors.map(errorMesage => <li>{errorMesage}</li>);

        return (
            <ul>
                {errorsList}
            </ul>
        );
    }

    render() {
        const { isVisible } = this.props;
        const { errors, password, username } = this.state;

        return (
            <Modal visible={isVisible}>
                <div className="login">
                    <div className="login__fields">
                        <TextField
                            className="login__text-field"
                            onChange={this.onUsernameChange}
                            placeholder="Username"
                            type="text"
                            value={username}
                        />
                        <TextField
                            className="login__text-field"
                            error={errors.length}
                            onChange={this.onPasswordChange}
                            placeholder="Password"
                            type="password"
                            value={password}
                        />
                    </div>
                    <div className="login__errors">
                        {this.renderErrors()}
                    </div>
                    <div className="login__button">
                        <Button
                            disabled={!username.length}
                            onClick={this.login}
                            primary
                        >
                            Login
                        </Button>
                    </div>
                </div>
            </Modal>
        );
    }
}

export default Login;
