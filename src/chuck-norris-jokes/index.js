import React, { Component } from 'react';

import Api from '../utils/api';
import JokesHelper from '../utils/jokes-helper';
import Lists from './lists';
import Login from './login';
import SessionHelper from '../utils/session-helper';
import {
    CATEGORIES,
    DEFAULT_SECONDS_TO_REFRESH,
    MAX_FETCHED_JOKES_NUMBER
} from '../utils/constants';

import './style.scss';

class ChuckNorrisJokes extends Component {
    state = {
        activeCategory: CATEGORIES.randomJokes,
        disableAutoRefresh: false,
        favorites: [],
        hasAutoRefresh: false,
        jokes: [],
        isUserLoggedIn: SessionHelper.isUserLoggedIn(),
        timeToRefresh: DEFAULT_SECONDS_TO_REFRESH
    }

    interval = undefined;

    getRandomJokes = () => {
        const { favorites } = this.state;

        Api
            .fetchJokesList(favorites)
            .then((jokes) => {
                this.setState({
                    jokes,
                    timeToRefresh: DEFAULT_SECONDS_TO_REFRESH
                });
            });
    }

    getSingleJoke = () => {
        const { favorites, jokes } = this.state;

        if (jokes.length === MAX_FETCHED_JOKES_NUMBER) {
            return this.setState({
                disableAutoRefresh: true,
                hasAutoRefresh: false
            });
        }

        const { timeToRefresh } = this.state;
        const isTimeOut = timeToRefresh === 1;

        if (!isTimeOut) {
            return this.setState({
                timeToRefresh: timeToRefresh - 1
            });
        }

        Api
            .fetchSingleJoke(jokes)
            .then((newJokes) => {
                this.setState({
                    jokes: newJokes,
                    timeToRefresh: DEFAULT_SECONDS_TO_REFRESH
                });
            });
    }

    onSuccesfulLogin = (user) => {
        SessionHelper.storeSession(user);

        this.setState({
            isUserLoggedIn: true
        });
    }

    selectFavorite = (joke) => {
        const { jokes, favorites } = this.state;
        const newFavoriteJokes = JokesHelper.addFavorite(favorites, joke);

        this.setState({
            favorites: newFavoriteJokes,
            jokes: JokesHelper.processJokes(jokes, newFavoriteJokes)
        });
    }

    setAutoRefresh = () => {
        const { hasAutoRefresh } = this.state;

        this.setState({
            hasAutoRefresh: !hasAutoRefresh, // Toggle checkbox
            timeToRefresh: DEFAULT_SECONDS_TO_REFRESH
        }, () => {
            const { hasAutoRefresh: newHasAutoRefresh } = this.state;
            // If new state is to have Auto-Refresh on
            if (newHasAutoRefresh) {
                this.interval = setInterval(this.getSingleJoke, 1000);
            } else {
                clearInterval(this.interval);
                this.interval = undefined;
            }
        });
    }

    showFavorites = () => {
        this.setState({
            activeCategory: CATEGORIES.favorites
        });
    }

    showRandomJokes = () => {
        this.setState({
            activeCategory: CATEGORIES.randomJokes
        });
    }

    render() {
        const {
            activeCategory,
            disableAutoRefresh,
            favorites,
            hasAutoRefresh,
            isUserLoggedIn,
            jokes,
            timeToRefresh
        } = this.state;

        return (
            <div className="chuck-norris-jokes">
                <Lists
                    disableAutoRefresh={disableAutoRefresh}
                    activeCategory={activeCategory}
                    favorites={favorites}
                    hasAutoRefresh={hasAutoRefresh}
                    jokes={jokes}
                    refreshJokesList={this.getRandomJokes}
                    selectFavorite={this.selectFavorite}
                    setAutoRefresh={this.setAutoRefresh}
                    showFavorites={this.showFavorites}
                    showRandomJokes={this.showRandomJokes}
                    timeToRefresh={timeToRefresh}
                />
                <Login
                    onSuccesfulLogin={this.onSuccesfulLogin}
                    isVisible={!isUserLoggedIn}
                />
            </div>
        );
    }
}

export default ChuckNorrisJokes;
